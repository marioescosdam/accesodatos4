package com.marioea.main;
import com.marioea.gui.Controlador;
import com.marioea.gui.Modelo;
import com.marioea.gui.Vista;
public class Principal {
    public static void main(String[] args) {
        Modelo modelo = new Modelo();
        Vista vista = new Vista();
        Controlador controlador = new Controlador(modelo, vista);
    }
}
