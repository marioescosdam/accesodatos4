package com.marioea.gui;

import com.marioea.util.Util;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Vector;

public class Controlador  implements ActionListener, ItemListener, ListSelectionListener, WindowListener {
    /**
     * @author Mario Escos
     * @version 1.8
     */
    private Modelo modelo;
    private Vista vista;
    boolean refrescar;
    //constructor de controlador

    /**
     *
     * @param modelo
     * @param vista
     */
    public Controlador(Modelo modelo, Vista vista) {
        this.modelo = modelo;
        this.vista = vista;
        modelo.conectar();
        setOptions();
        addActionListeners(this);
        addItemListeners(this);
        addWindowListeners(this);
        refrescarTodo();
    }

    /**
     * Metodo para almacenar todos los refrescar del proyecto
     * y crea un refrescar total
     */
    private void refrescarTodo() {
        refrescarTienda();
        refrescarCreador();
        refrescarJuguete();
        refrescar = false;
    }

    /**
     * le damos funcionalidad a cada boton que hemos creado anteriormente
     *  tambien le damos funcionalidad a los botoncitos del desplegable de arriba a la izquierda
     *   a la hora de ejecutar el programa
     * @param listener
     */
    private void addActionListeners(ActionListener listener) {
        vista.btnJugueteAnadir.addActionListener(listener);
        vista.btnCreadorAnadir.addActionListener(listener);
        vista.btnTiendaAnadir.addActionListener(listener);
        vista.btnJugueteEliminar.addActionListener(listener);
        vista.btnCreadorEliminar.addActionListener(listener);
        vista.btnTiendaEliminar.addActionListener(listener);
        vista.btnJugueteModificar.addActionListener(listener);
        vista.btnCreadorModificar.addActionListener(listener);
        vista.btnTiendaModificar.addActionListener(listener);
        vista.optionDialog.btnGuardarOpciones.addActionListener(listener);
        vista.itemOpciones.addActionListener(listener);
        vista.itemSalir.addActionListener(listener);
        vista.itemDesconectar.addActionListener(listener);
        vista.itemCrearTabla.addActionListener(listener);
        vista.btnValidate.addActionListener(listener);
        vista.btnBuscar.addActionListener(listener);
        vista.btnBuscarCreador.addActionListener(listener);
        vista.btnBuscarTienda.addActionListener(listener);
        vista.btnOrdenarJuguete.addActionListener(listener);
        vista.btnprocedimientoCreador.addActionListener(listener);
        vista.btnprocedimientoTienda.addActionListener(listener);
    }
    private void addWindowListeners(WindowListener listener) {
        vista.addWindowListener(listener);
    }
    /**
     * Muestra los atributos de un objeto seleccionado y los borra una vez se deselecciona
     * @param e Evento producido en una lista
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting()
                && !((ListSelectionModel) e.getSource()).isSelectionEmpty()) {
            if (e.getSource().equals(vista.tiendaTabla.getSelectionModel())) {
                int row = vista.tiendaTabla.getSelectedRow();
                vista.txtNombreTienda.setText(String.valueOf(vista.tiendaTabla.getValueAt(row, 1)));
                vista.txtNumeroTienda.setText(String.valueOf(vista.tiendaTabla.getValueAt(row, 2)));
                vista.txtEmailTienda.setText(String.valueOf(vista.tiendaTabla.getValueAt(row, 3)));
                vista.fechatienda.setDate((Date.valueOf(String.valueOf(vista.tiendaTabla.getValueAt(row, 4)))).toLocalDate());
                vista.txtNumeroVentasTienda.setText(String.valueOf(vista.tiendaTabla.getValueAt(row, 5)));
            } else if (e.getSource().equals(vista.creadorTabla.getSelectionModel())) {
                int row = vista.creadorTabla.getSelectedRow();
                vista.txtCreadorNombre.setText(String.valueOf(vista.creadorTabla.getValueAt(row, 1)));
                vista.txtCreadorDireccion.setText(String.valueOf(vista.creadorTabla.getValueAt(row, 2)));
                vista.comboCreadorPaises.setSelectedItem(String.valueOf(vista.creadorTabla.getValueAt(row, 3)));
                vista.txtCreadorCiudad.setText(String.valueOf(vista.creadorTabla.getValueAt(row, 4)));
                vista.txtTelefono.setText(String.valueOf(vista.creadorTabla.getValueAt(row, 5)));
                vista.fecha.setDate((Date.valueOf(String.valueOf(vista.creadorTabla.getValueAt(row, 6)))).toLocalDate());

            } else if (e.getSource().equals(vista.juguetesTabla.getSelectionModel())) {
                int row = vista.juguetesTabla.getSelectedRow();
                vista.txtNombre.setText(String.valueOf(vista.juguetesTabla.getValueAt(row, 1)));
                vista.comboCreador.setSelectedItem(String.valueOf(vista.juguetesTabla.getValueAt(row, 2)));
                vista.comboTienda.setSelectedItem(String.valueOf(vista.juguetesTabla.getValueAt(row, 3)));
                vista.comboTipoJuguete.setSelectedItem(String.valueOf(vista.juguetesTabla.getValueAt(row, 4)));
                vista.txtPrecio.setText(String.valueOf(vista.juguetesTabla.getValueAt(row, 5)));
                vista.comboSeccion.setSelectedItem(String.valueOf(vista.juguetesTabla.getValueAt(row, 6)));
                vista.comboCalidad.setSelectedItem(String.valueOf(vista.juguetesTabla.getValueAt(row, 7)));



            } else if (e.getValueIsAdjusting()
                    && ((ListSelectionModel) e.getSource()).isSelectionEmpty() && !refrescar) {
                if (e.getSource().equals(vista.tiendaTabla.getSelectionModel())) {
                    borrarDatosTienda();
                } else if (e.getSource().equals(vista.creadorTabla.getSelectionModel())) {
                    borrarDatosCreador();
                } else if (e.getSource().equals(vista.juguetesTabla.getSelectionModel())) {
                    borrarDatosJuguetes();
                }
            }
        }
    }  /**
     * aqui he creado un switch para añadirle la funcion
     * que van a desarrollar en cada caso
     * cada boton
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        switch (command) {
            case "Opciones":
                //ponemos visible el JDialog para cuando pulsemos opciones
                //se abra la ventana para introducir la contraseña
                //la cual hemos almacenado en el properties
                vista.adminPasswordDialog.setVisible(true);
                break;
            case "Desconectar":
                //cuando pulsemos desconectar se finalizara la conexion con la base
                modelo.desconectar();
                break;
            case "CrearTabla ":
                try {
                    //coge el metodo de modelo para crear las tablas
                    modelo.crearTablas();


                } catch (SQLException e1) {
                    e1.printStackTrace();
                }

                break;
            case "Salir":
                //unicamente es un system.exit para abandonar la cerrar el ejecutable
                System.exit(0);
                break;
            case "abrirOpciones":
                //si la contraseña del admin es valida unicmanete la comprueba
                if(String.valueOf(vista.adminPassword.getPassword()).equals(modelo.getAdminPassword())) {
                    vista.adminPassword.setText("");
                    vista.adminPasswordDialog.dispose();
                    vista.optionDialog.setVisible(true);
                } else {
                    //la contraseña no es valida
                    Util.showErrorAlert("La contraseña introducida no es valida.");
                }
                break;
            case "guardarOpciones":
                //te pide un usuario y contraseña de mysql por si la contraseña de mysql es distinta
                modelo.setPropValues(vista.optionDialog.tfIP.getText(), vista.optionDialog.tfUsuario.getText(),
                        String.valueOf(vista.optionDialog.pfContrasena.getPassword()), String.valueOf(vista.optionDialog.pfContrasena.getPassword()));
                vista.optionDialog.dispose();
                vista.dispose();
                new Controlador(new Modelo(), new Vista());
                break;
            case "añadirJuguete": {
                //nos advierte de rellenar todos los campos nos avisa tambien si el nombre esta en uso con ventanas
                try {
                    if (comprobarJugueteVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.juguetesTabla.clearSelection();
                    } else if (modelo.nombreJugueteYaExiste(vista.txtNombre.getText())) {
                        Util.showErrorAlert("Ese nombre ya esta en uso.\nIntroduce un nombre distinto");
                        vista.juguetesTabla.clearSelection();
                    } else {
                        //llama al metodo de modelo de crear un juguete y coge un texto o selecciona items por cada campo que poseemos en la tabla juguete
                        modelo.insertarJuguete(
                                vista.txtNombre.getText(),
                                String.valueOf(vista.comboCreador.getSelectedItem()),
                                String.valueOf(vista.comboTienda.getSelectedItem()),
                                String.valueOf(vista.comboTipoJuguete.getSelectedItem()),
                                Float.parseFloat(vista.txtPrecio.getText()),
                                Float.parseFloat(vista.txtNumeroLote.getText()),
                                String.valueOf(vista.comboSeccion.getSelectedItem()),
                                String.valueOf(vista.comboCalidad.getSelectedItem()));

                    }
                } catch (NumberFormatException nfe) {
                    //avisa de que no hemos introducido un valor numerico y hemos metido una palabra
                    Util.showErrorAlert("Introduce un valor numerico en el Precio y Numero de Lote");
                    vista.juguetesTabla.clearSelection();
                }
                //una vez realizado todoo eso borra los datos de juguete y refresca el juguete
                borrarDatosJuguetes();
                refrescarJuguete();
            }
            break;
            case "modificarJuguete": {
                try {
                    //nos advierte de rellenar todos los campos nos avisa tambien si el nombre esta en uso con ventanas
                    if (comprobarJugueteVacio()) {
                        Util.showErrorAlert("Rellena todos los datos");
                        vista.juguetesTabla.clearSelection();
                    } else {
                        //llama al metodo de modificar de modelo y modifica el campo que deseamos cambiar
                        modelo.modificarJuguete(
                                vista.txtNombre.getText(),
                                String.valueOf(vista.comboCreador.getSelectedItem()),
                                String.valueOf(vista.comboTienda.getSelectedItem()),
                                String.valueOf(vista.comboTipoJuguete.getSelectedItem()),
                                Float.parseFloat(vista.txtPrecio.getText()),
                                Float.parseFloat(vista.txtNumeroLote.getText()),
                                String.valueOf(vista.comboSeccion.getSelectedItem()),
                                String.valueOf(vista.comboCalidad.getSelectedItem()),
                                Integer.parseInt((String)vista.juguetesTabla.getValueAt(vista.juguetesTabla.getSelectedRow(), 0)));
                    }
                } catch (NumberFormatException nfe) {
                    //avisa de que no hemos introducido un valor numerico y hemos metido una palabra
                    Util.showErrorAlert("Introduce valores numericos donde sea estrictamente necesario");
                    vista.juguetesTabla.clearSelection();
                }
                //una vez realizado todoo eso borra los datos de juguete y refresca el juguete
                borrarDatosJuguetes();
                refrescarJuguete();
            }
            break;
            case "eliminarJuguete":
                //marcamos la linea que deseamos borrar le damos al boton de borrar y la fila desaparece
                modelo.borrarJuguete(Integer.parseInt((String)vista.juguetesTabla.getValueAt(vista.juguetesTabla.getSelectedRow(), 0)));
                borrarDatosJuguetes();
                refrescarJuguete();
                break;
            case "BuscarJuguete":
                //creamos un string de marca llamamos al txt de buscar y cogemos el texto que
                //hemos escrito
                String nombreJuguete=vista.txtBuscar.getText();
                try {
                    //llamamos al result set que se encarga de almacenar la consulta
                    //busca automaticamente el resultado mediante el metodo buscar
                    ResultSet rs =modelo.buscarJuguete(nombreJuguete);
                    //aqui carga las filas y restablece la tabla principal
                    // mostrando unicamente el resultado que hemos deseado buscar
                    cargarFilas(rs);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                break;
            case "OrdenarJuguete":
                //creamos un string de marca llamamos al txt de buscar y cogemos el texto que
                //hemos escrito
                String calidad=vista.txtOrdenarJuguete.getText();
                try {
                    //llamamos al result set que se encarga de almacenar la consulta
                    //busca automaticamente el resultado mediante el metodo buscar ordenado
                    ResultSet rs =modelo.ordenarDatos(calidad);
                    //aqui carga las filas y restablece la tabla principal
                    // mostrando unicamente el resultado que hemos deseado buscar
                    cargarFilas(rs);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                break;
            case "añadirCreador": {
                //nos advierte de rellenar todos los campos nos avisa tambien si el nombre esta en uso con ventanas
                try {
                    if (comprobarCreadorVacio()) {
                        Util.showErrorAlert("Rellena todos los datos");
                        vista.creadorTabla.clearSelection();
                    } else if (modelo.creadorNombreYaExiste(vista.txtCreadorNombre.getText(),
                            vista.txtCreadorNombre.getText())) {
                        Util.showErrorAlert("Ese nombre ya esta en uso.\nIntroduce un creador distinto");
                        vista.creadorTabla.clearSelection();
                    } else {
                        //llama al metodo de modelo de crear un juguete y coge un texto o selecciona items por cada campo que poseemos en la tabla creador
                        modelo.insertarCreador(vista.txtCreadorNombre.getText(),
                                vista.txtCreadorDireccion.getText(),
                                String.valueOf(vista.comboCreadorPaises.getSelectedItem()),
                                vista.txtCreadorCiudad.getText(),
                                vista.txtTelefono.getText(),
                                vista.fecha.getDate());
                        //refresca el creador
                        refrescarCreador();
                    }

                } catch (NumberFormatException nfe) {
                    //te dice que debes introducir numeros en los campos requeridos
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.creadorTabla.clearSelection();
                }
                borrarDatosCreador();
                refrescarCreador();
            }
            break;

            case "modificarCreador": {
                try {
                    if (comprobarCreadorVacio()) {
                        //rellena los datos
                        Util.showErrorAlert("Rellena todos los datos");
                        vista.creadorTabla.clearSelection();
                    } else {
                        //llama al metodo de modificar de modelo y modifica el campo que deseamos cambiar
                        modelo.modificarCreador(vista.txtCreadorNombre.getText(), vista.txtCreadorDireccion.getText(),
                                String.valueOf(vista.comboCreadorPaises.getSelectedItem()),
                                vista.txtCreadorCiudad.getText(),
                                vista.txtTelefono.getText(),
                                vista.fecha.getDate(),
                                Integer.parseInt((String)vista.creadorTabla.getValueAt(vista.creadorTabla.getSelectedRow(), 0)));
                        refrescarCreador();
                    }
                } catch (NumberFormatException nfe) {
                    //te dice que debes introducir numeros en los campos requeridos
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.creadorTabla.clearSelection();
                }
                //borra los datos del creador
                borrarDatosCreador();
            }
            break;
            case "eliminarCreador":
                //elimina la fila seleccionada
                modelo.borrarCreador(Integer.parseInt((String)vista.creadorTabla.getValueAt(vista.creadorTabla.getSelectedRow(), 0)));
                borrarDatosCreador();
                refrescarCreador();
                break;
            case "ProcedimientoCreador":
                try {
                    //coge el metodo de mostrar el id nombre pais ciudad cuando el pais sea alemania
                    modelo.NombrePaisCiudadCuandoPaisSeaAlemania();
                    //carga las filas y obtiene los datos para de esa manera mostrarlo en la tabla
                    cargarFilasCreador1(modelo.obtenerDatos1());
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                break;
            case "buscarCreador":
                //creamos un string de marca llamamos al txt de buscar y cogemos el texto que
                //hemos escrito
                String direccion=vista.txtBuscarCreador.getText();
                try {
                    //llamamos al result set que se encarga de almacenar la consulta
                    //busca automaticamente el resultado mediante el metodo buscar
                    ResultSet rs =modelo.buscarCreador(direccion);
                    //aqui carga las filas y restablece la tabla principal
                    // mostrando unicamente el resultado que hemos deseado buscar
                    cargarFilasCreador(rs);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                break;
            case "añadirTienda": {
                //nos advierte de rellenar todos los campos nos avisa tambien si el nombre esta en uso con ventanas
                try {
                    if (comprobarTiendaVacia()) {
                        Util.showErrorAlert("Rellena todos los datos");
                        vista.tiendaTabla.clearSelection();
                    } else if (modelo.tiendaNombreYaExiste(vista.txtNombreTienda.getText())) {
                        Util.showErrorAlert("Ese nombre ya existe.\nIntroduce un nombre distinto.");
                        vista.tiendaTabla.clearSelection();
                    } else {
                        //llama al metodo de modelo de crear un juguete y coge un texto o selecciona items por cada campo que poseemos en la tabla tienda
                        modelo.insertarTienda(vista.txtNombreTienda.getText(), vista.txtNumeroTienda.getText(),
                                vista.txtEmailTienda.getText(),
                                vista.fechatienda.getDate(),
                                Integer.parseInt(vista.txtNumeroVentasTienda.getText()));
                        //refreca la tabla
                        refrescarTienda();
                    }
                } catch (NumberFormatException nfe) {
                    //introduce numeros en los campos requeridos
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.tiendaTabla.clearSelection();
                }
                borrarDatosTienda();
            }
            break;
            case "modificarTienda": {
                try {
                    if (comprobarTiendaVacia()) {
                        Util.showErrorAlert("Rellena todos los datos");
                        vista.tiendaTabla.clearSelection();
                    } else {
                        //llama al metodo de modificar de modelo y modifica el campo que deseamos cambiar
                        modelo.modificarTienda(vista.txtNombreTienda.getText(), vista.txtNumeroTienda.getText(),
                                vista.txtEmailTienda.getText(),
                                vista.fechatienda.getDate(),
                                Integer.parseInt(vista.txtNumeroVentasTienda.getText()),
                                Integer.parseInt((String)vista.tiendaTabla.getValueAt(vista.tiendaTabla.getSelectedRow(), 0)));
                        refrescarTienda();
                    }
                } catch (NumberFormatException nfe) {
                    //requiere que introduzcas campos numericos
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.tiendaTabla.clearSelection();
                }
                borrarDatosTienda();
            }
            break;
            case "eliminarTienda":
                //elimina la fila seleccionada
                modelo.borrarTienda(Integer.parseInt((String)vista.tiendaTabla.getValueAt(vista.tiendaTabla.getSelectedRow(), 0)));
                borrarDatosTienda();
                refrescarTienda();
                break;
            case "buscarTienda":
                //creamos un string de marca llamamos al txt de buscar y cogemos el texto que
                //hemos escrito
                String numeroVentas=vista.txtBuscarTienda.getText();
                try {
                    //llamamos al result set que se encarga de almacenar la consulta
                    //busca automaticamente el resultado mediante el metodo buscar
                    ResultSet rs =modelo.buscarTienda(numeroVentas);
                    //aqui carga las filas y restablece la tabla principal
                    // mostrando unicamente el resultado que hemos deseado buscar
                    cargarFilasTienda(rs);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                break;
            case "ProcedimientoTienda":
                try {
                    //coge el metodo de mostrar el id nombre y telefono
                    // cuando las ventas sean mayores de 30 en la tabla tienda
                    modelo.IdNombreTelefono();
                    //carga las filas y obtiene los datos para de esa manera mostrarlo en la tabla
                    cargarFilasTienda1(modelo.obtenerDatos2());
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                break;
        }
    }

    /**
     *  metodo para cerrar la ventana
     * @param e
     */

    @Override
    public void windowClosing(WindowEvent e) {
        System.exit(0);
    }

    /**
     * Actualiza los datos de tienda que se ven en la lista y los comboboxes
     */
    private void refrescarTienda() {
        try {
            vista.tiendaTabla.setModel(construirTableModeloTienda(modelo.consultarTienda()));
            vista.comboTienda.removeAllItems();
            for(int i = 0; i < vista.dtmTienda.getRowCount(); i++) {
                vista.comboTienda.addItem(vista.dtmTienda.getValueAt(i, 0)+" - "+
                        vista.dtmTienda.getValueAt(i, 1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * metodo que contruye la tabla modelo de tienda con las columnas y filas correspondientes
     * @param rs
     * @return
     * @throws SQLException
     */
    private DefaultTableModel construirTableModeloTienda(ResultSet rs) throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();


        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }


        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        vista.dtmTienda.setDataVector(data, columnNames);

        return vista.dtmTienda;

    }
    /**
     * Actualiza los datos de creador que se ven en la lista y los comboboxes
     */
    private void refrescarCreador() {
        try {
            vista.creadorTabla.setModel(construirTableModeloCreador(modelo.consultarCreador()));
            vista.comboCreador.removeAllItems();
            for(int i = 0; i < vista.dtmCreador.getRowCount(); i++) {
                vista.comboCreador.addItem(vista.dtmCreador.getValueAt(i, 0)+" - "+
                        vista.dtmCreador.getValueAt(i, 2)+", "+vista.dtmCreador.getValueAt(i, 1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * metodo que contruye la tabla modelo de creador con las columnas y filas correspondientes
     * @param rs
     * @return
     * @throws SQLException
     */
    private DefaultTableModel construirTableModeloCreador(ResultSet rs) throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        vista.dtmCreador.setDataVector(data, columnNames);

        return vista.dtmCreador;

    }
    /**
     * Actualiza los datos de juguete que se ven en la lista y los comboboxes
     */
    private void refrescarJuguete() {
        try {
            vista.juguetesTabla.setModel(construirTableModeloJuguete(modelo.consultarJuguete()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    /**
     * metodo que contruye la tabla modelo de juguete con las columnas y filas correspondientes
     * @param rs
     * @return
     * @throws SQLException
     */
    private DefaultTableModel construirTableModeloJuguete(ResultSet rs)
            throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        vista.dtmJuguete.setDataVector(data, columnNames);

        return vista.dtmJuguete;

    }

    private void setDataVector(ResultSet rs, int columnCount, Vector<Vector<Object>> data) throws SQLException {
        while (rs.next()) {
            Vector<Object> vector = new Vector<>();
            for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
                vector.add(rs.getObject(columnIndex));
            }
            data.add(vector);
        }
    }
    /**
     * Setea el dialog de opciones según las opciones que el usuario guardo en la
     * última ejecución del programa.
     */
    private void setOptions() {
        vista.optionDialog.tfIP.setText(modelo.getIP());
        vista.optionDialog.tfUsuario.setText(modelo.getUser());
        vista.optionDialog.pfContrasena.setText(modelo.getPassword());
        vista.optionDialog.pfContrasenaAdmin.setText(modelo.getAdminPassword());
    }

    /**
     * Vacía los campos de la tabla de juguetes
     */
    private void borrarDatosJuguetes() {
        vista.txtNombre.setText("");
        vista.comboCreador.setSelectedIndex(-1);
        vista.comboTienda.setSelectedIndex(-1);
        vista.comboTipoJuguete.setSelectedIndex(-1);
        vista.txtPrecio.setText("");
        vista.txtNumeroLote.setText("");
        vista.comboSeccion.setSelectedIndex(-1);
        vista.comboCalidad.setSelectedIndex(-1);
    }
    /**
     * Vacía los campos de la tabla de creador
     */

    private void borrarDatosCreador() {
        vista.txtCreadorNombre.setText("");
        vista.txtCreadorDireccion.setText("");
        vista.comboCreadorPaises.setSelectedIndex(-1);
        vista.txtCreadorCiudad.setText("");
        vista.txtTelefono.setText("");
        vista.fecha.setText("");
    }

    /**
     * Vacía los campos de la tabla de tienda
     */
    private void borrarDatosTienda() {
        vista.txtNombreTienda.setText("");
        vista.txtNumeroTienda.setText("");
        vista.txtEmailTienda.setText("");
        vista.fechatienda.setText("");
        vista.txtNumeroVentasTienda.setText("");
    }
    /**
     * Comprueba que los campos necesarios para añadir un juguete estén vacíos
     *
     * @return True si al menos uno de los campos está vacío
     */
    private boolean comprobarJugueteVacio() {
        return vista.txtNombre.getText().isEmpty() ||
                vista.comboCreador.getSelectedIndex() == -1 ||
                vista.comboTienda.getSelectedIndex() == -1 ||
                vista.comboTipoJuguete.getSelectedIndex() == -1 ||
                vista.txtPrecio.getText().isEmpty() ||
                vista.txtNumeroLote.getText().isEmpty() ||
                vista.comboSeccion.getSelectedIndex() == -1 ||
                vista.comboCalidad.getSelectedIndex() == -1;
    }

    /**
     * Comprueba que los campos necesarios para añadir un creador estén vacíos
     *
     * @return True si al menos uno de los campos está vacío
     */
    private boolean comprobarCreadorVacio() {
        return vista.txtCreadorNombre.getText().isEmpty() ||
                vista.txtCreadorDireccion.getText().isEmpty() ||
                vista.comboCreadorPaises.getSelectedIndex() == -1 ||
                vista.txtCreadorCiudad.getText().isEmpty() ||
                vista.txtTelefono.getText().isEmpty() ||
                vista.fecha.getText().isEmpty();
    }

    /**
     * Comprueba que los campos necesarios para añadir una tienda estén vacíos
     *
     * @return True si al menos uno de los campos está vacío
     */
    private boolean comprobarTiendaVacia() {
        return vista.txtNombreTienda.getText().isEmpty() ||
                vista.txtNumeroTienda.getText().isEmpty() ||
                vista.txtEmailTienda.getText().isEmpty() ||
                vista.fechatienda.getText().isEmpty() ||
                vista.txtNumeroVentasTienda.getText().isEmpty();
    }
    /**
     *metodo que contiene el numero de fila con las que contara la tabla en este caso 9
     * @param resultSet
     * @throws SQLException
     */
    private void cargarFilas(ResultSet resultSet) throws SQLException {
        Object[] fila=new Object[9];
        vista.dtmJuguete.setRowCount(0);

        while (resultSet.next()){
            fila[0]=resultSet.getObject(1);
            fila[1]=resultSet.getObject(2);
            fila[2]=resultSet.getObject(3);
            fila[3]=resultSet.getObject(4);
            fila[4]=resultSet.getObject(5);
            fila[5]=resultSet.getObject(6);
            fila[6]=resultSet.getObject(7);
            fila[7]=resultSet.getObject(8);
            fila[8]=resultSet.getObject(9);

            vista.dtmJuguete.addRow(fila);
        }




    }
    /**
     *metodo que contiene el numero de fila con las que contara la tabla en este caso 7
     * @param resultSet
     * @throws SQLException
     */
    private void cargarFilasCreador(ResultSet resultSet) throws SQLException {
        Object[] fila=new Object[7];
        vista.dtmCreador.setRowCount(0);

        while (resultSet.next()){
            fila[0]=resultSet.getObject(1);
            fila[1]=resultSet.getObject(2);
            fila[2]=resultSet.getObject(3);
            fila[3]=resultSet.getObject(4);
            fila[4]=resultSet.getObject(5);
            fila[5]=resultSet.getObject(6);
            fila[6]=resultSet.getObject(7);


            vista.dtmCreador.addRow(fila);
        }




    }
    /**
     *metodo que contiene el numero de fila con las que contara la tabla en este caso 4
     * @param resultSet
     * @throws SQLException
     */
    private void cargarFilasCreador1(ResultSet resultSet) throws SQLException {
        Object[] fila=new Object[4];
        vista.dtmCreador.setRowCount(0);

        while (resultSet.next()){
            fila[0]=resultSet.getObject(1);
            fila[1]=resultSet.getObject(2);
            fila[2]=resultSet.getObject(3);
            fila[3]=resultSet.getObject(4);

            vista.dtmCreador.addRow(fila);
        }




    }
    /**
     *metodo que contiene el numero de fila con las que contara la tabla en este caso 6
     * @param resultSet
     * @throws SQLException
     */
    private void cargarFilasTienda(ResultSet resultSet) throws SQLException {
        Object[] fila=new Object[6];
        vista.dtmTienda.setRowCount(0);

        while (resultSet.next()){
            fila[0]=resultSet.getObject(1);
            fila[1]=resultSet.getObject(2);
            fila[2]=resultSet.getObject(3);
            fila[3]=resultSet.getObject(4);
            fila[4]=resultSet.getObject(5);
            fila[5]=resultSet.getObject(6);



            vista.dtmTienda.addRow(fila);
        }




    }
    /**
     *metodo que contiene el numero de fila con las que contara la tabla en este caso 3
     * @param resultSet
     * @throws SQLException
     */
    private void cargarFilasTienda1(ResultSet resultSet) throws SQLException {
        Object[] fila=new Object[3];
        vista.dtmTienda.setRowCount(0);

        while (resultSet.next()){
            fila[0]=resultSet.getObject(1);
            fila[1]=resultSet.getObject(2);
            fila[2]=resultSet.getObject(3);




            vista.dtmTienda.addRow(fila);
        }




    }
    private void addItemListeners(Controlador controlador) {
    }

    @Override
    public void windowOpened(WindowEvent e) {
    }

    @Override
    public void windowClosed(WindowEvent e) {
    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
    }

    @Override
    public void windowActivated(WindowEvent e) {
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
    }

    @Override
    public void itemStateChanged(ItemEvent e) {

    }

}
