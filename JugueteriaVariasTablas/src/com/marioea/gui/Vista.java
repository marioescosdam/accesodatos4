package com.marioea.gui;


import com.github.lgooddatepicker.components.DatePicker;
import com.marioea.base.enums.CalidadJuguete;
import com.marioea.base.enums.PaisCreador;
import com.marioea.base.enums.SeccionJuguete;
import com.marioea.base.enums.TiposJuguete;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

public class Vista extends JFrame{
    private final static String TITULOFRAME = "Jugueteria";
    private JPanel panel1;
    private JTabbedPane tabbedPane1;
     JTextField txtNombre;
     JComboBox comboCreador;
     JComboBox comboTienda;
     JTextField txtPrecio;
     JTextField txtNumeroLote;
     JComboBox comboSeccion;
     JComboBox comboCalidad;
     JButton btnJugueteAnadir;
     JButton btnJugueteModificar;
     JButton btnJugueteEliminar;
     JTable juguetesTabla;
     DatePicker fecha;
     JButton btnCreadorAnadir;
     JButton btnCreadorModificar;
     JButton btnCreadorEliminar;
     JTextField txtCreadorNombre;
     JTextField txtCreadorDireccion;
     JComboBox comboCreadorPaises;
    JTextField txtTelefono;
    JTextField txtCreadorCiudad;
     JTable creadorTabla;
    JButton btnTiendaAnadir;
     JButton btnTiendaModificar;
     JButton btnTiendaEliminar;
     JTable tiendaTabla;
     JTextField txtNombreTienda;
     JTextField txtNumeroTienda;
     JTextField txtEmailTienda;
     JTextField txtNumeroVentasTienda;
     JLabel etiquetaEstado;
     JComboBox comboTipoJuguete;
     DatePicker fechatienda;
     JButton btnBuscar;
    JTextField txtBuscar;
    JButton btnBuscarCreador;
    JTextField txtBuscarCreador;
    JButton btnBuscarTienda;
    JTextField txtBuscarTienda;
    JButton btnOrdenarJuguete;
    JTextField txtOrdenarJuguete;
    JButton btnprocedimientoCreador;
     JButton btnprocedimientoTienda;

    DefaultTableModel dtmJuguete;
    DefaultTableModel dtmCreador;
    DefaultTableModel dtmTienda;
    JMenuItem itemOpciones;
    JMenuItem itemDesconectar;
    JMenuItem itemSalir;
    JMenuItem itemCrearTabla;
    OptionDialog optionDialog;
    JDialog adminPasswordDialog;
    JButton btnValidate;
    JPasswordField adminPassword;

    public Vista(){
        super(TITULOFRAME);
        initFrame();
    }

    private void initFrame() {
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        this.pack();
        this.setVisible(true);
        this.setSize(new Dimension(this.getWidth()+200, this.getHeight()+100));
        this.setLocationRelativeTo(null);
        optionDialog = new OptionDialog(this);
        setMenu();
        setAdminDialog();
        setEnumComboBox();
        setTableModels();
    }
    /**
     * Inicializa los DefaultTableModel, los setea en sus respectivas tablas
     */
    private void setTableModels() {

        this.dtmJuguete = new DefaultTableModel();
        this.juguetesTabla.setModel(dtmJuguete);

        this.dtmCreador = new DefaultTableModel();
        this.creadorTabla.setModel(dtmCreador);

        this.dtmTienda = new DefaultTableModel();
        this.tiendaTabla.setModel(dtmTienda);
    }

    /**
     * Setea una barra de menus
     */
    private void setMenu(){
        JMenuBar barra = new JMenuBar();
        JMenu menu = new JMenu("Archivo");
        itemOpciones = new JMenuItem("Opciones");
        itemOpciones.setActionCommand("Opciones");
        itemDesconectar = new JMenuItem("Desconectar");
        itemDesconectar.setActionCommand("Desconectar");
        itemCrearTabla = new JMenuItem("Crear tabla");
        itemCrearTabla.setActionCommand("CrearTabla");
        itemSalir = new JMenuItem("Salir");
        itemSalir.setActionCommand("Salir");
        menu.add(itemOpciones);
        menu.add(itemDesconectar);
        menu.add(itemCrearTabla);
        menu.add(itemSalir);
        barra.add(menu);
        barra.add(Box.createHorizontalGlue());
        this.setJMenuBar(barra);
    }
    /**
     * Setea los items de los combos con sus respectivas enumeraciones
     */
    private void setEnumComboBox() {
        for(CalidadJuguete constant : CalidadJuguete.values()) { comboCalidad.addItem(constant.getValor()); }
        comboCalidad.setSelectedIndex(-1);

        for(PaisCreador constant : PaisCreador.values()) { comboCreadorPaises.addItem(constant.getValor()); }
        comboCreadorPaises.setSelectedIndex(-1);

        for(SeccionJuguete constant : SeccionJuguete.values()) { comboSeccion.addItem(constant.getValor()); }
        comboSeccion.setSelectedIndex(-1);
        for(TiposJuguete constant : TiposJuguete.values()) { comboTipoJuguete.addItem(constant.getValor()); }
        comboTipoJuguete.setSelectedIndex(-1);
    }
    /**
     * Setea un JDialog para introducir la contreña de administrador y poder configurar la base de datos
     */
    private void setAdminDialog() {
        btnValidate = new JButton("Validar");
        btnValidate.setActionCommand("abrirOpciones");
        adminPassword = new JPasswordField();
        adminPassword.setPreferredSize(new Dimension(100, 26));
        Object[] options = new Object[] {adminPassword, btnValidate};
        JOptionPane jop = new JOptionPane("Introduce la contraseña"
                , JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION, null, options);

        adminPasswordDialog = new JDialog(this, "Opciones", true);
        adminPasswordDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        adminPasswordDialog.setContentPane(jop);
        adminPasswordDialog.pack();
        adminPasswordDialog.setLocationRelativeTo(this);

    }


}
