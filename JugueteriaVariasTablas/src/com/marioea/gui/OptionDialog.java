package com.marioea.gui;

import javax.swing.*;
import java.awt.*;

public class OptionDialog extends JDialog{
    private JPanel panel1;
    JButton btnGuardarOpciones;
    JTextField tfIP;
    JTextField tfUsuario;
    JPasswordField pfContrasena;
    JPasswordField pfContrasenaAdmin;
    private Frame propietario;

    /**
     * Constructor de la clase
     * @param propietario
     */
    public OptionDialog(Frame propietario){
        super(propietario, "Opciones", true);
        this.propietario = propietario;
        initDialog();
    }

    /**
     * Inicializa el JDialog
     */
    private void initDialog() {
        this.setContentPane(panel1);
        this.panel1.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
        this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        this.pack();
        this.setSize(new Dimension(this.getWidth()+200, this.getHeight()));
        this.setLocationRelativeTo(propietario);
    }

}
