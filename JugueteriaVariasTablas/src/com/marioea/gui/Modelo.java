package com.marioea.gui;

import java.io.*;
import java.sql.*;
import java.time.LocalDate;
import java.util.Properties;

public class Modelo {
    private String ip;
    private String user;
    private String password;
    private String adminPassword;

    /**
     * Constructor de la clase, inicializa los ArrayLists,
     * carga los datos del fichero properties y setea isChanged a false.
     */
    public Modelo() {
        getPropValues();
    }

    String getIP() {
        return ip;
    }
    String getUser() {
        return user;
    }
    String getPassword() {
        return password;
    }
    String getAdminPassword() {
        return adminPassword;
    }


    private Connection conexion;

    /**
     * metodo para conectar con la base de datos mientras que carga datos como la contraseña y usuario de un fichero
     * por si otra persona va a trabajar con este proyecto tenga mayor facilidad para acceder a la base de datos
     */
    void conectar() {

        try {
            conexion = DriverManager.getConnection(
                    "jdbc:mysql://"+ip+":3306/jugueteria",user, password);
        } catch (SQLException sqle) {
            try {
                conexion = DriverManager.getConnection(
                        "jdbc:mysql://"+ip+":3306/",user, password);

                PreparedStatement statement = null;

                String code = leerFichero();
                String[] query = code.split("--");
                for (String aQuery : query) {
                    statement = conexion.prepareStatement(aQuery);
                    statement.executeUpdate();
                }
                assert statement != null;
                statement.close();

            } catch (SQLException | IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * metodo en el cual creamos las tablas si aun no existen
     * @throws SQLException
     */
    public void crearTablas() throws SQLException {
        conexion=null;
        //escribimos la url de nuestra bbdd nuestro user y nuestra clave
        conexion = DriverManager.getConnection(
                "jdbc:mysql://"+ip+":3306/",user, password);

        //llamamos al procedimiento que crea la tabla de juguete
        String sentenciaSql="call crearTablas()";
        CallableStatement procedimiento=null;
        //prepara la sentencia y la ejecuta
        procedimiento=conexion.prepareCall(sentenciaSql);
        procedimiento.execute();
    }

    /**
     * metodo que lee un fichero desde la aplicacion en vez desde el propio workbench o otros programas
     * @return
     * @throws IOException
     */
    private String leerFichero() throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader("jugueteria_java.sql"))) {
            String linea;
            StringBuilder stringBuilder = new StringBuilder();
            while ((linea = reader.readLine()) != null) {
                stringBuilder.append(linea);
                stringBuilder.append(" ");
            }

            return stringBuilder.toString();
        }
    }

    /**
     * unicamente sirve para cerrar la conexion con la base de datos
     */
    void desconectar() {
        try {
            conexion.close();
            conexion = null;
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        }
    }

    /**
     * metodo que sirve para insertar los datos en la tabla creador creamos una variable
     *  para cada dato a introducir
     * @param nombrecreador
     * @param direccion
     * @param pais
     * @param ciudad
     * @param telefono
     * @param fechacreacion
     */
    void insertarCreador(String nombrecreador, String direccion, String pais, String ciudad, String telefono, LocalDate fechacreacion) {
        String sentenciaSql = "INSERT INTO creador (nombrecreador, direccion, pais, ciudad, telefono, fechacreacion) VALUES (?, ?, ?, ?, ?, ?)";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombrecreador);
            sentencia.setString(2, direccion);
            sentencia.setString(3, pais);
            sentencia.setString(4, ciudad);
            sentencia.setString(5, telefono);
            sentencia.setDate(6, Date.valueOf(fechacreacion));
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     *  metodo que sirve para insertar los datos en la tabla tienda creamos una variable
     *   para cada dato a introducir
     * @param nombretienda
     * @param telefono
     * @param email
     * @param fechaventas
     * @param numeroventas
     */
    void insertarTienda(String nombretienda, String telefono, String email, LocalDate fechaventas, int numeroventas) {
        String sentenciaSql = "INSERT INTO tienda (nombretienda, telefono, email, fechaventas, numeroventas) VALUES (?, ?, ?, ?, ?)";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombretienda);
            sentencia.setString(2, telefono);
            sentencia.setString(3, email);
            sentencia.setDate(4, Date.valueOf(fechaventas));
            sentencia.setInt(5, numeroventas);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     *  metodo que sirve para insertar los datos en la tabla juguete creamos una variable
     *  para cada dato a introducir
     * @param nombrejuguete
     * @param creador
     * @param tienda
     * @param tipoJuguete
     * @param precio
     * @param numerolote
     * @param seccion
     * @param calidad
     */
    void insertarJuguete(String nombrejuguete, String creador, String tienda,String tipoJuguete, float precio, float numerolote, String seccion, String calidad) {
        String sentenciaSql = "INSERT INTO juguete (nombrejuguete, creador, tienda,tipoJuguetes , precio, numerolote, seccion, calidad) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement sentencia = null;

        int idcreador = Integer.valueOf(creador.split(" ")[0]);
        int idtienda = Integer.valueOf(tienda.split(" ")[0]);

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombrejuguete);
            sentencia.setInt(2, idcreador);
            sentencia.setInt(3, idtienda);
            sentencia.setString(4, tipoJuguete);
            sentencia.setFloat(5, precio);
            sentencia.setFloat(6, numerolote);
            sentencia.setString(7, seccion);
            sentencia.setString(8, calidad);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * metodo para modificar los datos de una tienda en el cual unicamente
     * podemos modificar el dato que nosotros deseamos de la tabla
     * @param nombretienda
     * @param telefono
     * @param email
     * @param fechaventas
     * @param numeroventas
     * @param idtienda
     */
    void modificarTienda(String nombretienda, String telefono, String email, LocalDate fechaventas, int numeroventas, int idtienda){

        String sentenciaSql = "UPDATE tienda SET nombretienda = ?, telefono = ?, email = ?, fechaventas = ?, numeroventas = ?" +
                "WHERE idtienda = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombretienda);
            sentencia.setString(2, telefono);
            sentencia.setString(3, email);
            sentencia.setDate(4, Date.valueOf(fechaventas));
            sentencia.setInt(5, numeroventas);
            sentencia.setInt(6, idtienda);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * metodo para modificar los datos de un creador en el cual unicamente
     *  podemos modificar el dato que nosotros deseamos de la tabla
     * @param nombrecreador
     * @param direccion
     * @param pais
     * @param ciudad
     * @param telefono
     * @param fechacreacion
     * @param idcreador
     */
    void modificarCreador(String nombrecreador, String direccion,String pais,String ciudad,String telefono, LocalDate fechacreacion, int idcreador){

        String sentenciaSql = "UPDATE creador SET nombrecreador = ?, direccion = ?, pais = ?, ciudad = ?,telefono = ?,fechacreacion = ?" +
                "WHERE idcreador = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombrecreador);
            sentencia.setString(2, direccion);
            sentencia.setString(3, pais);
            sentencia.setString(4, ciudad);
            sentencia.setString(5, telefono);
            sentencia.setDate(6, Date.valueOf(fechacreacion));
            sentencia.setInt(7, idcreador);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * metodo para modificar los datos de una tienda en el cual unicamente
     * podemos modificar el dato que nosotros deseamos de la tabla
     * @param nombrejuguete
     * @param creador
     * @param tienda
     * @param tipoJuguete
     * @param precio
     * @param numerolote
     * @param seccion
     * @param calidad
     * @param idjuguete
     */
    void modificarJuguete(String nombrejuguete, String creador, String tienda, String tipoJuguete, float precio, float numerolote, String seccion, String calidad, int idjuguete) {

        String sentenciaSql = "UPDATE juguete SET nombrejuguete = ?, creador = ?, tienda = ?, tipojuguetes = ?, precio = ?, " +
                "numerolote = ?, seccion = ?, calidad = ? WHERE idjuguete = ?";
        PreparedStatement sentencia = null;

        int idtienda = Integer.valueOf(tienda.split(" ")[0]);
        int idcreador = Integer.valueOf(creador.split(" ")[0]);

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombrejuguete);
            sentencia.setInt(2, idcreador);
            sentencia.setInt(3, idtienda);
            sentencia.setString(4, tipoJuguete);
            sentencia.setFloat(5, precio);
            sentencia.setFloat(6, numerolote);
            sentencia.setString(7, seccion);
            sentencia.setString(8, calidad);
            sentencia.setInt(9, idjuguete);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * metodo para eliminar una fila
     * elimina una fila cuando la id sea el valor sea el de la fila seleccionada
     * @param idtienda
     */
    void borrarTienda(int idtienda) {
        String sentenciaSql = "DELETE FROM tienda WHERE idtienda = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, idtienda);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * metodo para eliminar una fila
     * elimina una fila cuando la id sea el valor sea el de la fila seleccionada
     * @param idcreador
     */
    void borrarCreador(int idcreador) {
        String sentenciaSql = "DELETE FROM creador WHERE idcreador = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, idcreador);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * metodo para eliminar una fila
     * elimina una fila cuando la id sea el valor sea el de la fila seleccionada
     * @param idjuguete
     */
    void borrarJuguete(int idjuguete) {
        String sentenciaSql = "DELETE FROM juguete WHERE idjuguete = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, idjuguete);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }
    /**
     * Este metodo devuelve los datos de tienda guardados en la base de datos
     * @return consulta
     * @throws SQLException
     */
    ResultSet consultarTienda() throws SQLException {
        String sentenciaSql = "SELECT concat(idtienda) as 'ID', concat(nombretienda) as 'Nombre Tienda', concat(telefono) as 'Teléfono', " +
                "concat(email) as 'Email', concat(fechaventas) as 'Fecha Ventas', concat(numeroventas) as 'Numero de ventas' FROM tienda";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }
    /**
     * Este metodo devuelve los datos de creador guardados en la base de datos
     * @return consulta
     * @throws SQLException
     */
    ResultSet consultarCreador() throws SQLException {
        String sentenciaSql = "SELECT concat(idcreador) as 'ID', concat(nombrecreador) as 'Nombre Creador', concat(direccion) as 'Direccion', " +
                "concat(pais) as 'País', concat(ciudad) as 'Ciudad', concat(telefono) as 'Teléfono', concat(fechacreacion) as 'Fecha de Creacion' FROM creador";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     * Este metodo devuelve los datos de juguete guardados en la base de datos
     * @return consulta
     * @throws SQLException
     */
    ResultSet consultarJuguete() throws SQLException {
        String sentenciaSql = "SELECT concat(b.idjuguete) as 'ID', concat(b.nombrejuguete) as 'Nombre Juguete', " +
                "concat(e.idtienda, ' - ', e.idtienda) as 'Tienda', concat(a.idcreador, ' - ', a.direccion, ', ', a.nombrecreador) as 'Creador'," +
                "concat(b.tipojuguetes) as 'TipoJuguetes', " +
                "concat(b.precio) as 'Precio', " +
                "concat(b.numerolote) as 'Numero de Lote'," +
                "concat(b.seccion) as 'Seccion',"+
                "concat(b.calidad) as 'Calidad'"+
                " FROM juguete as b " +
                "inner join tienda as e on e.idtienda = b.tienda inner join " +
                "creador as a on a.idcreador = b.creador";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }
    /**
     * Lee el archivo de propiedades y setea los atributos pertinentes
     */
    private void getPropValues() {
        InputStream inputStream = null;
        try {
            Properties prop = new Properties();
            String propFileName = "config.properties";

            inputStream = new FileInputStream(propFileName);

            prop.load(inputStream);
            ip = prop.getProperty("ip");
            user = prop.getProperty("user");
            password = prop.getProperty("pass");
            adminPassword = prop.getProperty("admin");

        } catch (Exception e) {
            System.out.println("Exception: " + e);
        } finally {
            try {
                if (inputStream != null) inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     *  Actualiza las propiedades pasadas por parámetro del archivo de propiedades
     * @param ip
     * @param user
     * @param pass
     * @param adminPass
     */
    void setPropValues(String ip, String user, String pass, String adminPass) {
        try {
            Properties prop = new Properties();
            prop.setProperty("ip", ip);
            prop.setProperty("user", user);
            prop.setProperty("pass", pass);
            prop.setProperty("admin", adminPass);
            OutputStream out = new FileOutputStream("config.properties");
            prop.store(out, null);

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        this.ip = ip;
        this.user = user;
        this.password = pass;
        this.adminPassword = adminPass;
    }

    /**
     * Este metodo comprueba si el nombre de un juguete ya esta en la base de datos
     * @param nombre
     * @return
     */
    public boolean nombreJugueteYaExiste(String nombre) {
        String salesConsult = "SELECT existeNombreJuguete(?)";
        PreparedStatement function;
        boolean nombreExists = false;
        try {
            function = conexion.prepareStatement(salesConsult);
            function.setString(1, nombre);
            ResultSet rs = function.executeQuery();
            rs.next();

            nombreExists = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return nombreExists;
    }

    /**
     * Este metodo comprueba si el nombre de una tienda  ya esta en la base de datos
     * @param nombre nombre de la editorial
     * @return true si ya existe
     */
    public boolean tiendaNombreYaExiste(String nombre) {
        String editorialNameConsult = "SELECT existeNombreTienda(?)";
        PreparedStatement function;
        boolean nombreTiendaExists = false;
        try {
            function = conexion.prepareStatement(editorialNameConsult);
            function.setString(1, nombre);
            ResultSet rs = function.executeQuery();
            rs.next();

            nombreTiendaExists = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return nombreTiendaExists;
    }

    /**
     * Este metodo comprueba si el nombre de un creador ya esta en la base de datos
     * @param nombre
     * @param pais
     * @return
     */
    public boolean creadorNombreYaExiste(String nombre, String pais) {
        String nombreCompleto = pais + ", " + nombre;
        String ConsultaNombreCreador = "SELECT existeNombreCreador(?)";
        PreparedStatement function;
        boolean nombreCreadorExists = false;
        try {
            function = conexion.prepareStatement(ConsultaNombreCreador);
            function.setString(1, nombreCompleto);
            ResultSet rs = function.executeQuery();
            rs.next();

            nombreCreadorExists = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return nombreCreadorExists;
    }

    /**
     * Este metodo comprueba si el nombre de un juguete ya existe en la base de datos
     * @param nombreJuguete
     * @return
     * @throws SQLException
     */
    public ResultSet buscarJuguete(String nombreJuguete) throws SQLException {
        if( conexion == null)
            return null;

        if( conexion.isClosed())
            return null;
        //realizamos una consulta en la cual cogemos mostramos todos los datos cuando la marca
        //del juguete sea igual a la introducida
        String consulta = "SELECT * FROM juguete WHERE nombrejuguete = ?";
        PreparedStatement sentencia = null;

        sentencia = conexion.prepareStatement(consulta);

        sentencia.setString(1, String.valueOf(nombreJuguete));
        //ejecuta la sentecia y retorna un resultado
        ResultSet resultado = sentencia.executeQuery();

        return resultado;





    }

    /**
     * Este metodo comprueba si la direccion de un creador ya existe en la base de datos
     * @param direccion
     * @return
     * @throws SQLException
     */
    public ResultSet buscarCreador(String direccion) throws SQLException {
        if( conexion == null)
            return null;

        if( conexion.isClosed())
            return null;
        //realizamos una consulta en la cual cogemos mostramos todos los datos cuando la marca
        //del juguete sea igual a la introducida
        String consulta = "SELECT * FROM creador WHERE direccion = ?";
        PreparedStatement sentencia = null;

        sentencia = conexion.prepareStatement(consulta);
        sentencia.setString(1, String.valueOf(direccion));
        //ejecuta la sentecia y retorna un resultado
        ResultSet resultado = sentencia.executeQuery();

        return resultado;





    }

    /**
     * Este metodo comprueba si el numero de ventas de una tienda ya existe en la base de datos
     * @param numeroVentas
     * @return
     * @throws SQLException
     */
    public ResultSet buscarTienda(String numeroVentas) throws SQLException {
        if( conexion == null)
            return null;

        if( conexion.isClosed())
            return null;
        //realizamos una consulta en la cual cogemos mostramos todos los datos cuando la marca
        //del juguete sea igual a la introducida
        String consulta = "SELECT * FROM tienda WHERE numeroventas = ?";
        PreparedStatement sentencia = null;

        sentencia = conexion.prepareStatement(consulta);
        sentencia.setString(1, String.valueOf(numeroVentas));
        //ejecuta la sentecia y retorna un resultado
        ResultSet resultado = sentencia.executeQuery();

        return resultado;





    }

    /**
     * metodo que busca todos los datos de un juguete por calidad y los ordena por precio
     * @param calidad
     * @return
     * @throws SQLException
     */
    public ResultSet ordenarDatos(String calidad) throws SQLException {
        if (conexion==null) {
            return null;
        }
        if (conexion.isClosed()) {
            return null;
        }
        //realizamos una consulta para coger todos los datos de juguete
        String consulta ="SELECT * FROM juguete WHERE calidad =? ORDER BY precio";
        PreparedStatement sentencia = null;
        //prepara la consulta y la ejecuta y retorna el resultado obtenido
        sentencia=conexion.prepareStatement(consulta);
        sentencia.setString(1, String.valueOf(calidad));
        ResultSet resultado=sentencia.executeQuery();
        return resultado;
    }

    /**
     * metodo que recoge la consulta y lo utilizamos para obtener los datos para la tabla
     * creador
     * @return
     * @throws SQLException
     */
    public ResultSet obtenerDatos1() throws SQLException {
        if (conexion==null) {
            return null;
        }
        if (conexion.isClosed()) {
            return null;
        }
        //realizamos una consulta para coger el tipo de juguete y la marca
        //cuando la calidad es buena
        String consulta =" SELECT idcreador,nombrecreador, pais, ciudad FROM creador WHERE  pais='alemania'";
        PreparedStatement sentencia = null;
        //prepara la consulta y la ejecuta y retorna el resultado obtenido
        sentencia=conexion.prepareStatement(consulta);
        ResultSet resultado=sentencia.executeQuery();
        return resultado;
    }

    /**
     * metodo que recoge la consulta y lo utilizamos para obtener los datos para la tabla
     * tienda
     * @return
     * @throws SQLException
     */
    public ResultSet obtenerDatos2() throws SQLException {
        if (conexion==null) {
            return null;
        }
        if (conexion.isClosed()) {
            return null;
        }
        //realizamos una consulta para coger el tipo de juguete y la marca
        //cuando la calidad es buena
        String consulta =" SELECT idtienda,nombretienda,telefono FROM tienda WHERE numeroventas>30";
        PreparedStatement sentencia = null;
        //prepara la consulta y la ejecuta y retorna el resultado obtenido
        sentencia=conexion.prepareStatement(consulta);
        ResultSet resultado=sentencia.executeQuery();
        return resultado;
    }

    /**
     * metodo que realiza un procedimiento
     * que muestra el nombre pais y creador cuando el pais sea alemania
     * @throws SQLException
     */
    public void NombrePaisCiudadCuandoPaisSeaAlemania() throws SQLException {
        String sentenciaSql="call mostrarNombrePaisCiudadCuandoPaisSeaAlemania()";
        CallableStatement procedimiento=null;
        procedimiento=conexion.prepareCall(sentenciaSql);
        //llama a la sentencia y la ejecuta
        procedimiento.execute();
    }

    /**
     * metodo que realiza un procedimiento
     * que muestra la id el nombre y el telefono cuando el numero de ventas sea mayor de 30
     * @throws SQLException
     */
    public void IdNombreTelefono() throws SQLException {
        String sentenciaSql="call mostrarIdNombreTelefono()";
        CallableStatement procedimiento=null;
        procedimiento=conexion.prepareCall(sentenciaSql);
        //llama a la sentencia y la ejecuta
        procedimiento.execute();
    }
}


