package com.marioea.base.enums;




/**
 * Esta clase enum enumera las constantes con las que se rellena
 * el JComoboBox comboPaisCreador de la vista.
 * Representan los paises que hemos almacenado
 */
public enum PaisCreador {
    PAISESTADOSUNIDOS("ESTADOS UNIDOS"),
    PAISCHINA("CHINA"),
    PAISRUSIA("RUSIA"),
    PAISALEMANIA("ALEMANIA"),
    PAISREINOUNIDO("REINO UNIDO"),
    PAISFRANCIA("FRANCIA"),
    PAISJAPON("JAPON"),
    PAISESPANA("ESPAÑA");


    private String valor;

    PaisCreador(String valor) {
        this.valor = valor;
    }

    public String getValor() {
        return valor;
    }
}
