package com.marioea.base.enums;
/**
 * Esta clase enum enumera las constantes con las que se rellena
 * el JComoboBox comboTiposJuguete de la vista.
 * Representan los tipos de juegos que hemos almacenado.
 */
public enum TiposJuguete {
    MESA("JUEGOS DE MESA"),
    DEPORTE("DEPORTE"),
    AZAR("JUEGOS DE AZAR"),
    CONSTRUCCION("CONSTRUCCIÓN"),
    EXTERIORES("EXTERIORES"),
    TRADICIONALES("TRADICIONALES");


    private String valor;

    TiposJuguete(String valor) {
        this.valor = valor;
    }

    public String getValor() {
        return valor;
    }
}
