package com.marioea.base.enums;


/**
 * Esta clase enum enumera las constantes con las que se rellena
 * el JComoboBox comboSeccion de la vista.
 * Representan las diferentes secciones a la hora de elegir un juguete por edad
 */
public enum SeccionJuguete {
    JUGUETESFAMILIAR("FAMILIAR"),
    JUGUETESPRIMERAINFANCIA("PRIMERAINFANCIA"),
    JUGUETESINFANTILES("INFANTILES"),
    JUGUETESADOLESCENTES("ADOLESCENTES"),
    JUGUETESJUVENIL("JUVENIL"),
    JUGUETESADULTO("ADULTO"),
    JUGUETESPERSONASMAYORES("PERSONASMAYORES");


    private String valor;

    SeccionJuguete(String valor) {
        this.valor = valor;
    }

    public String getValor() {
        return valor;
    }
}
