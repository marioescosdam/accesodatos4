package com.marioea.base.enums;


/**
 * Esta clase enum enumera las constantes con las que se rellena
 * el JComoboBox comboCalidadJuguete de la vista.
 * Representan las calidades de un juguete que existen.
 */
public enum CalidadJuguete {
    CALIDADMALA("MALA"),
    CALIDADREGULAR("REGULAR"),
    CALIDADDECENTE("DECENTE"),
    CALIDADBUENA("BUENA");


    private String valor;

    CalidadJuguete(String valor) {
        this.valor = valor;
    }

    public String getValor() {
        return valor;
    }
}
