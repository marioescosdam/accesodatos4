CREATE DATABASE if not exists jugueteria;
--
USE jugueteria;
--
create table if not exists creador(
                                      idcreador int auto_increment primary key,
                                      nombrecreador varchar(100) not null,
                                      direccion varchar(200) not null,
                                      pais varchar(100) not null,
                                      ciudad varchar (200) not null,
                                      telefono varchar(9),
                                      fechacreacion date);
--
create table if not exists tienda (
                                      idtienda int auto_increment primary key,
                                      nombretienda varchar(100) not null,
                                      telefono varchar(9),
                                      email varchar(200) not null,
                                      fechaventas date,
                                      numeroventas int not null);
--
create table if not exists juguete(
                                      idjuguete int auto_increment primary key,
                                      nombrejuguete varchar(100) not null,
                                      creador int not null,
                                      tienda int not null,
                                      tipojuguetes varchar(80),
                                      precio float not null,
                                      numerolote float not null,
                                      seccion varchar(100) not null,
                                      calidad varchar(100) not null);
--
alter table juguete
    add foreign key (tienda) references tienda(idtienda),
    add foreign key (creador) references creador(idcreador);
--
delimiter ||
create function existeNombreJuguete(nombrej varchar(40))
    returns bit
begin
    declare i int;
    set i = 0;
    while ( i < (select max(idjuguete) from juguete)) do
            if  ((select nombrejuguete from juguete where idjuguete = (i + 1)) like nombrej) then return 1;
            end if;
            set i = i + 1;
        end while;
    return 0;
end; ||
delimiter ;
--
delimiter ||
create function existeNombreTienda(nombret varchar(40))
    returns bit
begin
    declare i int;
    set i = 0;
    while ( i < (select max(idtienda) from tienda)) do
            if  ((select nombretienda from tienda where idtienda = (i + 1)) like nombret) then return 1;
            end if;
            set i = i + 1;
        end while;
    return 0;
end; ||
delimiter ;
--
delimiter ||
create function existeNombreCreador(nombrec varchar(100))
    returns bit
begin
    declare i int;
    set i = 0;
    while ( i < (select max(idcreador) from creador)) do
            if  ((select nombrecreador from creador where idcreador = (i + 1)) like nombrec) then return 1;
            end if;
            set i = i + 1;
        end while;
    return 0;
end; ||
delimiter ;
DELIMITER ||
CREATE PROCEDURE mostrarNombrePaisCiudadCuandoPaisSeaAlemania()
BEGIN
    SELECT idcreador,nombrecreador, pais, ciudad FROM creador WHERE  pais='alemania';
end; ||
delimiter ;
DELIMITER ||
CREATE PROCEDURE mostrarIdNombreTelefono()
BEGIN
    SELECT idtienda,nombretienda,telefono FROM tienda WHERE numeroventas>30;
end; ||
delimiter ;
delimiter ||

CREATE PROCEDURE crearTablas()
BEGIN
    CREATE TABLE juguete(
                            idjuguete int auto_increment primary key,
                            nombrejuguete varchar(100) not null,
                            creador int not null,
                            tienda int not null,
                            tipojuguetes varchar(80),
                            precio float not null,
                            numerolote float not null,
                            seccion varchar(100) not null,
                            calidad varchar(100) not null);
    CREATE TABLE tienda (
            idtienda int auto_increment primary key,
            nombretienda varchar(100) not null,
            telefono varchar(9),
            email varchar(200) not null,
            fechaventas date,
            numeroventas int not null);
    CREATE TABLE creador(
            idcreador int auto_increment primary key,
            nombrecreador varchar(100) not null,
            direccion varchar(200) not null,
            pais varchar(100) not null,
            ciudad varchar (200) not null,
            telefono varchar(9),
            fechacreacion date);


end; ||
delimiter ;
